Sliced Filament Dryer Spacer
============================

I have a dehydrator (a Nesco FD-61) that takes 13.5" trays...far too large
for most 3D printers.  Slic3r will cut models on the Z-axis, but after
adjusting the original dryer spacer for my dehydrator, I didn't see a way to
cut it into pieces.

Fortunately, since it's in OpenSCAD, we can create a "cheese wheel" that
will be subtracted from the spacer to cut it into a section small enough to
print.  On my Anet A8, I can cut a 90-degree section and print four of them
(and all four will fit with not much room to spare).

In addition to the existing variables, ANGLE can be set to something that'll
fit on your printer:

360 = whole
180 = half
120 = third
90 = fourth
72 = fifth
60 = sixth

You'll need a newer build of OpenSCAD for this to work, as the angle
parameter to rotate_extrude() was only added sometime in 2016.  The last
release was in 2015, so this means you'll need a development build from
http://www.openscad.org/downloads.html#snapshots.
